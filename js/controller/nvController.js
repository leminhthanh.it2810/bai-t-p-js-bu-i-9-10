function laythongtintuForm() {
    // Lấy thông tin từ form
    const _tkNv = document.getElementById('tknv').value;
    const _tenNv = document.getElementById('name').value;
    const _email = document.getElementById('email').value;
    const _matKhau = document.getElementById('password').value;
    const _ngaylam = document.getElementById('datepicker').value;
    const _luongcoban = document.getElementById('luongCB').value;
    const _chucvu = document.getElementById('chucvu').value;
    const _giolam = document.getElementById('gioLam').value;

    // Tạo object nv
    var nv = new NhanVien(_tkNv,_tenNv,_matKhau,_email,_ngaylam,_luongcoban,_chucvu,_giolam);
    return nv;
}

function renderDSNV(nvArr) {
    //  render nvArr ra table

    var contentHTML = "";
    for (var index = 0; index < nvArr.length; index++) {
        var item = nvArr[index];
        var contentTr = `<tr>
                            <td>${item.taikhoan}</td>
                            <td>${item.ten}</td>
                            <td>${item.email}</td>
                            <td>${item.ngaylam}</td>
                            <td>${item.chucvu}</td>
                            <td>0</td>
                            <td>0</td>
                            <td>
                                <button onClick="xoaNhanVien('${item.taikhoan}')" class="btn btn-danger">Xóa</button>
                            </td>
                            <td>
                                <button data-toggle="modal" data-target="#myModal" onClick="suaNhanVien('${item.taikhoan}')" class="btn btn-warning">Sửa</button>
                            </td>
                        </tr>`;
        contentHTML += contentTr;
    }

    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function timKiemViTri(id, arr) {
    var vitri = -1;
    for (var index = 0; index < arr.length; index++) {
        var nv = arr[index];
        if (nv.taikhoan == id) {
            vitri = index;
            break;
        }
    }
    return vitri;
}

function showTongTinLenForm(nv) {
    document.getElementById('tknv').value = nv.taikhoan;
    document.getElementById('name').value = nv.ten;
    document.getElementById('email').value = nv.email;
    document.getElementById('password').value = nv.matKhau;
    document.getElementById('datepicker').value = nv.ngaylam;
    document.getElementById('luongCB').value = nv.luongcoban;
    document.getElementById('chucvu').value = nv.chucvu;
    document.getElementById('gioLam').value = nv.giolam;
}