var DSNV = "DSNV";
var dsnv = [];
var dsnvjson = localStorage.getItem(DSNV);
if(dsnvjson != null){
    var nvArr = JSON.parse(dsnvjson);
    dsnv =nvArr.map(function (item){
        var nv = new NhanVien(item.taikhoan, item.ten, item.matKhau, item.email, item.ngaylam, item.luongcoban, item.chucvu, item.giolam);
        return nv;
    })
    renderDSNV(dsnv);
}

function themNhanVien() {
    var nv = laythongtintuForm();

    dsnv.push(nv);
    var dsnvjson = JSON.stringify(dsnv);
    localStorage.setItem(DSNV, dsnvjson);
    renderDSNV(dsnv);
}

function xoaNhanVien(idNv) {
    //    Tìm vị trí

    var vitri = timKiemViTri(idNv, dsnv);

    if (vitri != -1) {
        dsnv.splice(vitri, 1)
        // render lại layout sau khi xóa thành công
        renderDSNV(dsnv);
    }

}

function suaNhanVien(idNv) {
    var vitri = timKiemViTri(idNv, dsnv);

    if (vitri == -1) {
        // dừng chương trình nếu ko tìm thấy
        return;
    }
    var nv = dsnv[vitri];
    // show thông tin lên form
    showTongTinLenForm(nv)
}

function capNhatNhanVien() {
    // Lấy thông tin sau khi update
    var nv = laythongtintuForm();
    // update dữ liệu mới thay thế dữ liệu cũ
    var vitri = timKiemViTri(nv.taikhoan, dsnv);
    if (vitri != -1) {
        dsnv[vitri] = nv;
        var dsnvjson = JSON.stringify(dsnv);
        localStorage.setItem(DSNV, dsnvjson);
        renderDSNV(dsnv);
    }
}